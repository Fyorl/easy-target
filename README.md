A small [Foundry VTT](https://foundryvtt.com/) module that allows for easy targeting whilst holding alt. Also allows for alt+clicking inside an AoE to target all tokens within that AoE. Holding alt while placing a template will also target all tokens within that template when placed.

Clear all targets with alt+shift+c, or ctrl+shift+c on Mac.
